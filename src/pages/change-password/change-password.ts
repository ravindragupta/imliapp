import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { GlobalService } from '../../app.service';
import { TabsPage } from '../tabs/tabs';
import { NavController, ToastController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'change-password',
    templateUrl: 'change-password.html'
})

export class ChangePasswordComponent {

    changePassword: FormGroup;
    userId: any;
    errorMsg: string;
    translate: any = { changePass: '', oldPass: '', newPass: '', confirmPass: '' };

    constructor(private ga: GoogleAnalytics, public toastCtrl: ToastController, public navCntl: NavController, public formBuilder: FormBuilder, public base_path_service: GlobalService) {
        this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter change password page', 'Password change', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        switch (localStorage.getItem("lang")) {
            case 'hi': this.translate.changePass = "पासवर्ड बदलें"; this.translate.oldPass = "पुराना पासवर्ड";
                this.translate.newPass = "नया पासवर्ड"; this.translate.confirmPass = "पासवर्ड की पुष्टि कीजिये";
                break;
            case 'en': this.translate.changePass = "Change Passwprd"; this.translate.oldPass = "Old Password";
                this.translate.newPass = "New Password"; this.translate.confirmPass = "Confirm Password";
                break;
        }
        this.userId = localStorage.getItem('userId');
        this.formCode();
    }

    formCode() {
        this.changePassword = this.formBuilder.group({
            oldPassword: ['', [Validators.required]],
            newPassword: ['', [Validators.required]],
            confirmPassword: ['', [Validators.required]]
        })
    }

    presentToast() {
        let toast = this.toastCtrl.create({
            message: this.errorMsg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }

    // passwordMatch(control: FormControl) {
    //     return (control.value == this.changePassword.value.confirmPassword) ? null : { passwordMatch: true }
    // }

    changePasswordApi() {
        let data = {
            key: this.userId,
            oldPassword: this.changePassword.controls['oldPassword'].value,
            newPassword: this.changePassword.controls['newPassword'].value
        }
        let url = this.base_path_service.base_path + 'user/changePassword';
        this.base_path_service.PostRequest(url, data)
            .subscribe(data => {
                console.log("success");
                setTimeout(() => {
                    this.ga.startTrackerWithId('UA-103017766-1')
                        .then(() => {
                            console.log('Google analytics is ready now');
                            this.ga.trackEvent('Change password', 'Password Changed');
                        })
                        .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
                    this.navCntl.setRoot(TabsPage);
                })
            },
            err => {
                let check = JSON.parse(err._body);
                this.errorMsg = check.message;
                console.log("asdf", check.message);
                this.presentToast();
                this.changePassword.reset();
            });
    }

}