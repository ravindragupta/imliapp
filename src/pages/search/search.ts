import { Component } from '@angular/core';
import { SearchUser } from '../searchUser/searchUser';
import { NavController, NavParams, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import { SeePostByCategoryPage } from '../seePostByCategory/seePostByCategory';
import { SettingsPage } from '../settings/settings';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  getCategoryList: any
  getCategoryList2: any
  categoryName: string;
  subCategory: string;
  hideFeature: boolean = false;
  exploreText: string;
  userId: string;
  urlsImg: string = 'https://sharechat.sia.co.in/userPhoto-1500016774593.png';

  constructor(private ga: GoogleAnalytics,private base_path_service: GlobalService, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    this.getCategory();
    this.userId = localStorage.getItem("userId");
    switch (localStorage.getItem("lang")) {
      case 'hi': this.exploreText = "एक्स्प्लोर";
        break;
      case 'en': this.exploreText = "Explore";
        break;
    }
    this.ga.startTrackerWithId('UA-103017766-1')
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackView('Enter Search post by category', 'Search Post', true);
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));
    // this.getCategory();
  }

  openSearchModel() {
    let modal = this.modalCtrl.create(SearchUser);
    modal.present();
  }

  navigateSettingPage() {
    this.navCtrl.push(SettingsPage);
  }

  ionViewDidEnter() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    loader.present();
    this.getCategory();
  }

  // presentModal(event) {
  //   console.log(event._id, "asdd");
  //   localStorage.setItem("postId", event._id);
  //   let modal = this.modalCtrl.create(CommentModel);
  //   modal.present();
  // }

  getCategory() {
    let url = this.base_path_service.base_path + 'admin/allCategory/' + this.userId + '?language=hi,en';
    this.base_path_service.GetRequest(url)
      .subscribe(data => {
        console.log("data", data);
        this.getCategoryList = data[0].json.hindiCategory;
        this.getCategoryList2 = data[0].json.englishCategory;
      },
      err => {
        console.log('some error', err);
      });
  }

  getSubCategoryName(event, category, subCat) {
    localStorage.setItem("category", category._id);
    localStorage.setItem("subCategory", subCat._id);
    this.navCtrl.push(SeePostByCategoryPage);
    console.log("check", this.categoryName, this.subCategory);
  }

}
