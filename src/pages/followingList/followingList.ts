import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'page-followingList',
    templateUrl: 'followingList.html'
})

export class FollowingList {

    userId: string;
    flowersData: any;

    constructor(private ga: GoogleAnalytics,public loadingCtrl: LoadingController, private base_path_service: GlobalService, public navCtrl: NavController) {
        let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 2000
        });
        loader.present();
        this.userId = localStorage.getItem("userId");
        this.searchUserList();
          this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter Followers page', 'See followers list', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));  
    }

    searchUserList() {
        let url = this.base_path_service.base_path + 'user/viewFollowers/' + this.userId;
        this.base_path_service.GetRequest(url)
            .subscribe(data => {
                console.log("followers list", data);
                this.flowersData = data[0].json.data;
            },
            err => {
                console.log('some error', err);
            });
    }
}