import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { EditProfilePage } from '../editProfile/editProfile';
// import { LoginPage } from '../loginPage/loginPage';
import { GlobalService } from '../../app.service';
import { SettingsPage } from '../settings/settings';
import { ChooseLangPage } from '../chooseLang/chooseLang';
import { FeedbackPage } from '../feedback/feedback';
import { MyPostPage } from '../myPost/myPost';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { FollowTabsPage } from "../followTabs/followTabs";
// import { FollowingList } from "../followingList/followingList";
// import { YouList } from "../you/you";
// import { HomeTabsPage } from "../homeTabs/tabs";
// import { HomePage } from "../home/home";


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  userId: string;
  lang: string;
  profileDetails: any;
  profileDetailsNodata: any;
  hideFeature: boolean = false;
  profileImage: string;
  translate: any = { profileText: '', notification: '', editProfile: '', posting: '', followers: '', following: '', logout: '' };
  translate2: any = { name: '' };


  constructor(private ga: GoogleAnalytics, public base_path_service: GlobalService, public modalCtrl: ModalController, private alertCtrl: AlertController, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams) {
    this.selectLang();
  }

  selectLang(){
    this.ga.startTrackerWithId('UA-103017766-1')
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackView('Enter Profile page', 'Profile', true);
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));
    switch (localStorage.getItem("lang")) {
      case 'hi': this.translate.profileText = "प्रोफाइल"; this.translate.notification = "नोटिफिकेशन";
        this.translate.editProfile = "प्रोफाइल एडिट करें"; this.translate.posting = "पोस्ट";
        this.translate.followers = "फोल्लोवेर्स"; this.translate.following = "फोल्लोविंग";
        this.translate.logout = "लॉग आउट";
        this.translate2.name = 'प्रतिक्रिया';
        this.translate2.toggle = 'अंग्रेज़ी में बदले';

        break;
      case 'en': this.translate.profileText = "Profile"; this.translate.notification = "Notification";
        this.translate.editProfile = "Edit Profile"; this.translate.posting = "Post";
        this.translate.followers = "Followers"; this.translate.following = "Following";
        this.translate.logout = "Log Out";
        this.translate2.name = 'Feedback';
        this.translate2.toggle = 'Switch to Hindi'
        break;
    }
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    this.userId = localStorage.getItem("userId");
    this.lang = localStorage.getItem("lang");
    this.viewProfile();
  }
  ionViewDidEnter() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    loader.present();
    console.log('ionViewDidLoad FollowFeed');
    this.viewProfile();
  }

  navigateSettingPage() {
    this.navCtrl.push(SettingsPage);
  }

  navigateTofeedbackPage() {
    this.navCtrl.push(FeedbackPage);
  }


  navigateEditProfilePage() {
    this.navCtrl.push(EditProfilePage);
  }

  presentModal() {
    let modal = this.modalCtrl.create(ChooseLangPage);
    modal.present();
  }

  logOut() {
    localStorage.removeItem("token");
    localStorage.removeItem("userId");
    this.navCtrl.pop();
    this.presentModal();
  }

  myPostpage() {
    this.navCtrl.push(MyPostPage)
  }

  myFollowersPage(){
    this.navCtrl.push(FollowTabsPage,{select: 'FollowingList'});
  }

  myFollowingPage(){
    this.navCtrl.push(FollowTabsPage,{select: 'YouList'});
  }


  viewProfile() {
    let url = this.base_path_service.base_path + 'user/viewProfile/' + this.userId;
    this.base_path_service.GetRequest(url)
      .subscribe(data => {
        this.profileDetails = data[0].json.data;
        this.profileDetailsNodata = data[0].json;
        this.profileImage = this.profileDetails.profilePic;
        // document.getElementById('profile').style.backgroundImage = 'url('+this.profileDetails.profilePic+')';
        console.log("view profile", this.profileDetails.profilePic);
      },
      err => {
        console.log('some error', err);
      });
  }

  switchTo(lang:any){
    switch(lang){
      case 'hi': localStorage.setItem("lang",'en');
      break;
      case 'en': localStorage.setItem("lang",'hi');
    }
    let url = this.base_path_service.base_path + 'user/languageToggle';
    let data= {
      key: localStorage.getItem('userId'),
      language: localStorage.getItem('lang')
    }
    this.base_path_service.PutRequest(url, data)
    .subscribe(
      res => {console.log(res);},
      err => {console.log(err);}
    )
    
    this.selectLang();
    // this.navCtrl.setRoot(this.navCtrl.getActive().component);
    // this.navCtrl.parent.select(0);
    setTimeout(function(){
    window.location.reload();
    },400);
  }

}
