import { Component,NgZone } from "@angular/core";
import {
  Http,
  Request,
  RequestMethod,
  Response,
  RequestOptions
} from "@angular/http";
import {
  NavController,
  AlertController,
  ModalController,
  LoadingController,
} from "ionic-angular";
import { GlobalService } from "../../app.service";
import { ChooseCategoryPage } from "../chooseCategory/chooseCategory";
import { GoogleAnalytics } from "@ionic-native/google-analytics";

@Component({
  selector: "page-createPost",
  templateUrl: "createPost.html"
})
export class CreatePostPage {
  imgArr = [];


  employeeImg: File;
  imageUrl: any;
  imgDiv: boolean = false;
  optionsBoxOpen: boolean;
  optionsBoxData: any;
  onSubmit: boolean = true;
  userId: string;
  status: string;
  categoryName: string;
  subCategory: string;
  lang: string;
  loader: any;
  createPostText: string;
  msgtext: string;
  checkPost: boolean = false;
  logo_src: File;

  constructor(
    private ga: GoogleAnalytics,
    public loadingCtrl: LoadingController,
    public base_path_service: GlobalService,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private http: Http,
    private _zone: NgZone
  ) {
    this.ga
      .startTrackerWithId("UA-103017766-1")
      .then(() => {
        console.log("Google analytics is ready now");
        this.ga.trackView("Enter Create post page", "Feedback", true);
      })
      .catch(e => console.log("Error starting GoogleAnalytics", e));
    localStorage.removeItem("postImg");
    localStorage.removeItem("postMsg");

    switch (localStorage.getItem("lang")) {
      case "hi":
        this.createPostText = "पोस्ट बनाएं";
        this.msgtext = "यहां पोस्ट करें";
        break;
      case "en":
        this.createPostText = "Create Post";
        this.msgtext = "Write your post here...";
        break;
    }
    this.lang = localStorage.getItem("lang");
    this.userId = localStorage.getItem("userId");
    
  }

  presentLoader() {
    this.loader = this.loadingCtrl.create({
      content: "Uploading Please wait..."
    });
    this.loader.present();
  }

  presentModal(event) {
    let modal = this.modalCtrl.create(ChooseCategoryPage);
    modal.onDidDismiss(res=>{
      this.navCtrl.pop();
    })
    modal.present();
  }

  setMsg() {
    if (this.status == null) this.checkPost = false;
    else this.checkPost = true;
    localStorage.setItem("postMsg", this.status);
  }


  fileChangeEvent_first(fileInput: any) {
    this.employeeImg = fileInput.target.files[0];
    // var preview = <HTMLImageElement>document.getElementById('logo');
    var reader1 = new FileReader();
    if (this.employeeImg) {
      reader1.readAsDataURL(this.employeeImg);
    }
    console.log(this.employeeImg, "check");
    this.imageUpload();
 
  }

  ionViewDidLeave() {
    this.imageUrl = null;
    this.imgDiv = false;
    console.log("exiting", this.imgDiv, this.imageUrl);
  }

  imageUpload() {
    this.presentLoader();
    let url = this.base_path_service.base_path + "user/uploadImage";
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      formData.append("userPhoto", this.employeeImg);
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200 || xhr.status == 201) {
            localStorage.setItem("postImg", xhr.response);
            this._zone.run(()=>{
              // document.getElementById("myImage").style.backgroundImage ='url('+xhr.response+')';
            this.imageUrl = xhr.response;
            })
            this.imgDiv = true;
            this.checkPost = true;
            console.log(xhr, "xhr....");

            console.log("check img url", this.imageUrl, this.imgDiv);
            this.loader.dismiss();
          } else if (xhr.status != 200 && xhr.status != 201) {
            this.loader.dismiss();
            console.log(xhr.response + " else if xhr");
          }
        }
      };
      xhr.open("Post", url, true);
      console.log(xhr.open("Post", url, true));
      // xhr.setRequestHeader( 'Content-Type','multipart/form-data' );
      // xhr.setRequestHeader("Authorization", 'Bearer ' + access_token);
      xhr.send(formData);
      console.log("yesa");
    });
  }

}