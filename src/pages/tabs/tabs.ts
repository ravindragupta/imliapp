import { Component } from "@angular/core";
import { NavController, Platform, NavParams } from "ionic-angular";

import { AboutPage } from "../about/about";
import { HomePage } from "../home/home";
import { SearchPage } from "../search/search";
import { SavedPostPage } from "../savedPost/savedPost";
import { CreatePostPage } from "../createPost/createPost";
import { GoogleAnalytics } from "@ionic-native/google-analytics";

@Component({
  selector: "page-tabsOne",
  templateUrl: "tabs.html"
})
export class TabsPage {
  index: any;
  tab1Root = HomePage;
  tab2Root = SearchPage;
  tab3Root = CreatePostPage;
  tab4Root = SavedPostPage;
  tab5Root = AboutPage;

  constructor(
    private ga: GoogleAnalytics,
    public platform: Platform,
    public navParams: NavParams,
    public navCtrl: NavController
  ) {
    platform.registerBackButtonAction(() => {
      navigator["app"].exitApp();
    });
    this.ga
      .startTrackerWithId("UA-103017766-1")
      .then(() => {
        console.log("Google analytics is ready now");
        this.ga.trackView("FollowFeed", "follow-feed.html", true);
      })
      .catch(e => console.log("Error starting GoogleAnalytics", e));
  }

  ionViewDidEnter() {
    this.index = this.navParams.get("index")
      ? this.navParams.get("index")
      : "0";
      console.log(this.index,"dgvjsbgvjkfdvbg");
      
  }

  navigateCreatePostPage() {
    this.navCtrl.push(CreatePostPage);
  }
}
