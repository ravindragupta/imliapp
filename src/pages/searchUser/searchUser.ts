import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import { FormControl } from '@angular/forms';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'page-userSearch',
    templateUrl: 'searchUser.html'
})

export class SearchUser implements OnInit {

    search_user: boolean = false;
    search_keyword = new FormControl('');
    userData: any;
    userId: any;
    followBoolean: boolean = false;
    translate: any = { name: '', later: '', submit: '' };
    followText: string;
    unfollowText: string;

    constructor(private ga: GoogleAnalytics, private base_path_service: GlobalService, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
        switch (localStorage.getItem("lang")) {
            case 'hi': this.translate.name = "प्रोफाइल खोजें"; this.translate.later = "नाम या निक नाम के साथ खोजें";
                this.translate.submit = 'जमा करें'; this.followText = " फॉलो"; this.unfollowText = " अनफॉलो";
                break;
            case 'en': this.translate.name = "Search Profile"; this.translate.later = "Search with name or nick name";
                this.translate.submit = 'SUBMIT'; this.followText = " Follow"; this.unfollowText = " Unfollow";
                break;
        }
        this.userId = localStorage.getItem("userId");
        this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter Search user page', 'Search User', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
    }
    ngOnInit() {
        /*For search*/
        (<any>this.search_keyword).valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .subscribe(
            (value: any) => {
                if (value.length > 2) {
                    this.searchUserList(value);
                }
            });
        /*For search*/
    }

    popNav() {
        this.navCtrl.pop();
    }

    searchUserList(value: any) {
        console.log("hitt444");
        let url = this.base_path_service.base_path + 'user/search/' + value + '/' + this.userId;
        this.base_path_service.GetRequest(url)
            .subscribe(data => {
                this.ga.startTrackerWithId('UA-103017766-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackView('Search user', value, true);
                    })
                    .catch(e => console.log('Error starting GoogleAnalytics', e));
                console.log("user list", data);
                this.userData = data[0].json.name;
                this.search_user = true;
            },
            err => {
                console.log('some error', err);
            });
    }

    follow(event, flag) {
        switch (flag) {
            case 'follow': event.isFollow = true;
                break;
            case 'unfollow': event.isFollow = false;
                break;
        }
        let data = {
            key: this.userId,
            followingId: event._id
        }
        let url = this.base_path_service.base_path + 'user/following';
        this.base_path_service.PostRequest(url, data)
            .subscribe(data => {
                this.ga.startTrackerWithId('UA-103017766-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackView('follow user in', 'Search user page', true);
                    })
                    .catch(e => console.log('Error starting GoogleAnalytics', e));
                console.log("follow list", data);
            },
            err => {

                console.log('some error', err);
            });
    }
}