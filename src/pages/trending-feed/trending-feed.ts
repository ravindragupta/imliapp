import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import { CommentPage } from '../commentPage/commentPage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

// import { CommentModel } from '../comment/comment-model';
// import { ViewPostPage } from '../viewPost/viewPost';
// import { ViewAllUserProfilePage } from '../viewAllUserProfile/viewAllUserProfile';



@Component({
  selector: 'page-trending-feed',
  templateUrl: 'trending-feed.html',
})

export class TrendingFeed {

  userId: any;
  trending: any;
  display: boolean = false;
  like: boolean = false;
  save: boolean = false;
  isSave: boolean = false;
  likeValue: boolean;
  postId: string;
  followingText: string;
  followtext: string;
  likeTxt: string;
  viewAllcmt: string;
  tippdi: string;
  shareTx: string;
  page: number = 1;
  totalCount: number;
  dummy: string = "dont";
  viewTxt: string;
  eventItemText: any = { like: '', comment: '', share: '', save: '' };

  constructor(private ga: GoogleAnalytics, private socialSharing: SocialSharing, public alertCtrl: AlertController, public base_path_service: GlobalService, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams) {
    switch (localStorage.getItem("lang")) {
      case 'hi': this.viewTxt = 'देखने वाले', this.shareTx = " शेयर "; this.viewAllcmt = " सभी को देखें "; this.tippdi = " टिप्पणियाँ "; this.likeTxt = "को यह पसंद है"; this.followingText = "फोल्लोविंग"; this.followtext = '+ फॉलो';
        this.eventItemText.like = 'लाइक'; this.eventItemText.share = 'शेयर करें'; this.eventItemText.save = 'सेव करें';
        this.eventItemText.comment = 'कहिये';
        break;
      case 'en': this.viewTxt = 'views', this.shareTx = " Share "; this.viewAllcmt = " View all "; this.tippdi = " Comments "; this.likeTxt = "Likes"; this.followingText = "Following"; this.followtext = '+ Follow';
        this.eventItemText.like = 'Like'; this.eventItemText.share = 'Share'; this.eventItemText.save = 'Save';
        this.eventItemText.comment = 'Comment';
    }
    this.userId = localStorage.getItem("userId");
    this.trendindList();
    this.ga.startTrackerWithId('UA-103017766-1')
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackView('Trending Feed Page');
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrendingFeed');
  }

  presentModal(event) {
    console.log(event._id, "asdd");
    localStorage.setItem("postId", event._id);
    let modal = this.modalCtrl.create(CommentPage);
    modal.present();
  }

  shareViaWhatsApp(data) {
    console.log("call share", data.resource);
    this.socialSharing.shareViaWhatsApp('Imli khatta meetha', data.resource, data.resource).then(res => {
      console.log("res", res);
    }).catch((err) => {
      console.log(err, "some error");
    });
  }

  // presentModalForViewPost(postData) {
  //   localStorage.setItem("viewPostId", postData._id);
  //   localStorage.setItem("viewUserId", postData.userId);
  //   let modal = this.modalCtrl.create(ViewPostPage);
  //   modal.present();
  // }

  // viewuserProfiles() {
  //   let modal = this.modalCtrl.create(ViewAllUserProfilePage);
  //   modal.present();
  // }

  ionViewDidEnter() {
    this.trending = [];
    this.page = 1;
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    loader.present();
    this.userId = localStorage.getItem("userId");
    this.trendindList(this.dummy);
  }

  doInfinite(event) {
    console.log("on bottom scroll", event);
    if (this.page < this.totalCount) {
      this.page++;
      this.trendindList(event);
    }
    else
      event.enable(false);

  }

  doRefresh(event) {
    console.log("check on pull", event);
    let url = this.base_path_service.base_path + 'user/viewTrendingData/' + this.userId + '?limit=10&page=' + 1;
    this.base_path_service.GetRequest(url)
      .subscribe(data => {
        // console.log("followers list", data);
        this.trending = data[0].json.data;
        setTimeout(() => {
          this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
              console.log('Google analytics is ready now');
              this.ga.trackEvent('Refresh -- Trending feed page', 'load new data', 'complete loading', 42);
            })
            .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
          // console.log('Async operation has ended');
          if (event == 'dont') {
            return;
          }
          else {
            event.complete();
          }
        }, 2000);
      },
      err => {
        console.log("some error", err);
      })
  }

  trendindList(event?) {
    let url = this.base_path_service.base_path + 'user/viewTrendingData/' + this.userId + '?limit=10&page=' + this.page;
    this.base_path_service.GetRequest(url)
      .subscribe(data => {
        let arr = data[0].json.data;
        if (this.page == 1) {
          console.log("feed data");
          this.trending = data[0].json.data;
        }
        else {
          this.trending = this.trending.concat(arr);
          if (event == 'dont') {
            return;
          }
          else {
            event.complete();
          }
        }
        this.totalCount = data[0].json.totalPages;
        // this.like = false;
        // this.save = false;
      },
      err => {
        console.log('some error', err);
      });
  }

  opneMore(event) {
    this.postId = event._id;
    if (event.isDelete) {
      switch (localStorage.getItem("lang")) {
        case 'hi': this.openSecondHindi(event);
          break;
        case 'en': this.openSecond(event);
          break;
      }
    }
    else {
      switch (localStorage.getItem("lang")) {
        case 'hi': this.openThirdHindi(event);
          break;
        case 'en': this.openThird(event);
          break;
      }
    }
    console.log("check boolean", event.isDelete);
  }

  openSecond(event) {
    let prompt = this.alertCtrl.create({
      title: 'Action',
      buttons: [
        {
          text: 'Share on facebook',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Report',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        },
        {
          text: 'Delete',
          handler: data => {
            this.deletePost(event);
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  openThird(event) {
    let prompt = this.alertCtrl.create({
      title: 'Action',
      buttons: [
        {
          text: 'Share on facebook',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Report',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  // ================== hindi ======================
  openSecondHindi(event) {
    let prompt = this.alertCtrl.create({
      title: 'मेनू',
      buttons: [
        {
          text: 'फेसबुक पर शेयर करें',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'रिपोर्ट',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        },
        {
          text: 'डिलीट',
          handler: data => {
            this.deletePost(event);
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  openThirdHindi(event) {
    let prompt = this.alertCtrl.create({
      title: 'मेनू',
      buttons: [
        {
          text: 'फेसबुक पर शेयर करें',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'रिपोर्ट',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }
  // =============== end hindi ++++++++++++++

  showCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Reason');

    alert.addInput({
      type: 'radio',
      label: 'Spam',
      value: 'spam',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Noneveg',
      value: 'noneveg'
    });

    alert.addInput({
      type: 'radio',
      label: 'Not intrested',
      value: 'notIntrested'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Okay',
      handler: data => {
        console.log('Checkbox data:', data);
        this.report(data)
      }
    });
    alert.present();
  }

  savePost(event) {
    if (event.isSave) {
      event.isSave = false;
    }
    else {
      event.isSave = true;
    }
    let data = {
      key: this.userId,
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/savePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        console.log("edit profle", data);
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Trending feed -- Save post', 'saved', 'Saved successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        // this.trendindList(this.dummy);
      },
      err => {
        this.save = false;
        console.log('some error', err);
      });
  }

  onLike(event) {
    console.log("asdff", event.liker);
    if (event.liker) {
      event.like.pop()
      event.liker = false;
    }
    else {
      event.like.push('123')
      event.liker = true;
    }
    let data = {
      key: this.userId,
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/likePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        console.log("edit profle", data);
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Trending feed -- Like post', 'LIKE', 'Like Post successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        // this.trendindList(this.dummy);
      },
      err => {
        this.like = false;
        console.log('some error', err);
      });
  }

  deletePost(event) {
    let data = {
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/deletePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        this.doRefresh(this.dummy);
        console.log("edit profle", data);
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Trending feed -- Delete post', 'Delete', 'Delete Post successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
      },
      err => {
        console.log('some error', err);
      });
  }

  report(reason) {
    let data = {
      postId: this.postId,
      type: reason,
      key: this.userId
    }
    let url = this.base_path_service.base_path + 'user/report';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        console.log("set report", data);
        this.trendindList(this.dummy);
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Trending feed -- Report post', reason, 'complete report', 12);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
      },
      err => {
        console.log('some error', err);
      });
  }

  follow(event) {
    if (event.isFollow) {
      event.isFollow = false;
    }
    else {
      event.isFollow = true;
    }
    console.log("ffff", event);
    let data = {
      key: this.userId,
      followingId: event.userId
    }
    let url = this.base_path_service.base_path + 'user/following';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        console.log("follow list", data);
        this.trendindList(this.dummy);
      },
      err => {
        console.log('some error', err);
      });
  }

}
