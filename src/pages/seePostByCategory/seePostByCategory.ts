import { Component } from '@angular/core';
import { ViraltabsPage } from '../viralTabs/viralTabs';

@Component({
    selector: 'page-seePostByCategory',
    templateUrl: 'seePostByCategory.html'
})

export class SeePostByCategoryPage {

    rootPage: any = ViraltabsPage;
    viralText: string;

    constructor() {
        switch (localStorage.getItem("lang")) {
            case 'hi': this.viralText = "वायरल";
                break;
            case 'en': this.viralText = "Viral";
                break;
        }
    }
}