import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalService } from '../../app.service';
import { ChangePasswordComponent } from '../change-password/change-password';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

// import _ from 'jimp';

@Component({
    selector: 'page-editProfile',
    templateUrl: 'editProfile.html'
})

export class EditProfilePage {

    username: string = "ravindragupta";
    mobile: string = "8578953525";
    location: string = "location here";
    editForm: FormGroup;
    employeeImg: File;
    imageUrl: string;
    userId: any;
    hindiCheck: boolean;
    englishCheck: boolean;
    onValidForm: boolean = false;
    imgArr = [];
    translate: any = { profileText: '', lang: '', changePass: '', change: '' };
    eighteenText: string;

    constructor(private ga: GoogleAnalytics, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public navCtrl: NavController, private formBuilder: FormBuilder, private base_path_service: GlobalService) {
        this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter Edit Profile page', 'Edit Profile', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        // this.asd();
        switch (localStorage.getItem("lang")) {
            case 'hi': this.eighteenText = '18+ पोस्ट देखें'; this.translate.profileText = "प्रोफाइल एडिट करें"; this.translate.lang = "भाषा";
                this.translate.changePass = "पासवर्ड बदलें"; this.translate.change = "बदलें";
                break;
            case 'en': this.eighteenText = 'See 18+ post'; this.translate.profileText = "Edit Profile"; this.translate.lang = "Language";
                this.translate.changePass = "Change Password"; this.translate.change = "Change"
                break;
        }
        this.userId = localStorage.getItem("userId");
        this.formCode();
        this.getUserDetails();
        this.editForm.controls['key'].setValue(localStorage.getItem("userId"));
    }

    formCode() {
        this.editForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
            userStatus: ['', [Validators.required]],
            profilePic: ['http://www.scii.in/Canvas/images/Power.png', [Validators.required]],
            key: '',
            eighteenPlus: false
        });
    }

    getLang(string) {
        switch (string) {
            case 'hi': this.changeLanguage('hi');
                break;
            case 'en': this.changeLanguage('en');
                break;
        }
    }

    // asd() {
    //     _.read("./assets/images/user.jpg", function (err, lenna) {
    //         if (err) throw err;
    //         lenna.resize(120, 120)            
    //             .quality(60)                 
    //             .greyscale()                
    //             .write("lena-small-bw.jpg"); 
    //     });
    // }

    changeLanguage(lang) {
        console.log("asdfaf");
        let data = {
            key: this.userId,
            multiLanguage: lang
        }
        let url = this.base_path_service.base_path + 'user/multiLanguage';
        this.base_path_service.PutRequest(url, data).
            subscribe(data => {
                this.ga.startTrackerWithId('UA-103017766-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackEvent('change language', 'Edit profile');
                    })
                    .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
                // if(da/ta)
                if (data[0].json.json().data.multiLanguage.length < 1) {
                    this.hindiCheck = true;
                }
                console.log("done", data[0].json);
                console.log("done2", data[0].json.json());
                console.log("done3", data[0].json.json().data.lastUpdate);
            },
            err => {
                console.log("error", err);
            })
    }

    updateUser() {
        let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 1000
        });
        loader.present();
        console.log("check1234", this.imgArr);
        let data = {
            key: this.userId,
            name: this.editForm.controls['name'].value,
            mobile: this.editForm.controls['mobile'].value,
            profilePic: this.imgArr,
            eighteenPlus: this.editForm.controls['eighteenPlus'].value
        }
        console.log("sd", data);
        let url = this.base_path_service.base_path + 'user/updateProfile'
        this.base_path_service.PutRequest(url, data)
            .subscribe(data => {
                this.ga.startTrackerWithId('UA-103017766-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackEvent('Edit profile', 'Update profile');
                    })
                    .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
                // this.dialogs.alert('Hello world');
                let toast = this.toastCtrl.create({
                    message: 'User Updated successfully',
                    position: 'top',
                    duration: 3000
                });
                toast.present();
            },
            err => {
                console.log('some error', err);
            });
    }

    getUserDetails() {
        let url = this.base_path_service.base_path + 'user/userDetails/' + this.userId;
        this.base_path_service.GetRequest(url)
            .subscribe(data => {
                let values: any = data[0].json.data;
                this.editForm.controls['name'].setValue(values.name);
                this.editForm.controls['mobile'].setValue(values.mobile);
                this.editForm.controls['profilePic'].setValue(values.profilePic);
                this.imageUrl = data[0].json.data.profilePic;
                document.getElementById('myImage').style.backgroundImage = 'url(' + data[0].json.data.profilePi + ')';
                this.imgArr.push(data[0].json.data.profilePic);
                let lang = data[0].json.data.multiLanguage;
                this.editForm.controls['eighteenPlus'].setValue(values.eighteenPlus);
                console.log("azz", lang);
                lang.forEach((ell) => {
                    switch (ell) {
                        case 'hi': this.hindiCheck = true;
                            break;
                        case 'en': this.englishCheck = true;
                            break;
                    }
                })
            },
            err => {
                console.log('some error', err);
            });
    }


    fileChangeEvent_first(fileInput: any) {
        this.employeeImg = fileInput.target.files[0];
        var preview = <HTMLImageElement>document.getElementById('logo');
        var reader1 = new FileReader();
        if (this.employeeImg) {
            reader1.readAsDataURL(this.employeeImg);
        }
        this.imageUpload();
        console.log(this.employeeImg, "check");
        // else if(this.employeeImg != undefined){
        //   this.imageUpload();
        // }
    }


    imageUpload() {
        let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 2000
        });
        loader.present();
        let url = this.base_path_service.base_path + "user/uploadImage";
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("userPhoto", this.employeeImg);
            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200 || xhr.status == 201) {
                        this.imgArr.push(xhr.response);
                        document.getElementById('myImage').style.backgroundImage = 'url(' + xhr.response + ')';
                        this.imageUrl = xhr.response;
                        this.ga.startTrackerWithId('UA-103017766-1')
                            .then(() => {
                                console.log('Google analytics is ready now');
                                this.ga.trackEvent('New Image upload', 'Edit profile');
                            })
                            .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
                        console.log("image uploaded", this.imgArr);
                        this.editForm.controls['profilePic'].setValue(this.imageUrl);
                    }
                    else if (xhr.status != 200 && xhr.status != 201) {
                        console.log(xhr.response + " else if xhr");
                    }
                }
            }
            xhr.open("Post", url, true);
            // xhr.setRequestHeader( 'Content-Type','multipart/form-data' );           
            // xhr.setRequestHeader("Authorization", 'Bearer ' + access_token);
            xhr.send(formData);
        });
    }

    navigateToChangePasswordPage() {
        this.navCtrl.push(ChangePasswordComponent);
    }

}
