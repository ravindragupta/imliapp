import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomeTabsPage } from '../homeTabs/tabs';
import { JwtHelper } from 'angular2-jwt';
import { SettingsPage } from '../settings/settings';
import { GlobalService } from "../../app.service";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  rootTabs: any = HomeTabsPage;
  tok = new JwtHelper();
  items: any;
  hideFeature: boolean = false;
  imliText: string;
  smalltext: string;
  followFeedText: string;
  trendingText: string;

  constructor(public navCtrl: NavController, public base_path_service: GlobalService) {
    switch (localStorage.getItem("lang")) {
      case 'hi': this.imliText = "इमली";
        this.smalltext = "खट्टा मीठा";
        this.followFeedText = "मेरा मोहल्ला";
        this.trendingText = "लोकप्रिय";
        break;
      case 'en': this.imliText = "Imli";
        this.smalltext = "Khatta Meetha";
    }
    let token = localStorage.getItem('token');
    this.items = this.tok.decodeToken(token);
    console.log("details", this.items);
    console.log("details", this.items.userId);
    localStorage.setItem("userId", this.items.userId);

        let url = this.base_path_service.base_path + 'user/languageToggle';
        let data= {
        key: localStorage.getItem('userId'),
        language: localStorage.getItem('lang')
        }
        this.base_path_service.PutRequest(url, data)
        .subscribe(
        res => {console.log(res);},
        err => {console.log(err);}
        )
  }

  navigateSettingPage() {
    this.navCtrl.push(SettingsPage);
  }

}
