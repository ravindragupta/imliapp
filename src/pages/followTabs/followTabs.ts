import { Component, ViewChild } from "@angular/core";

import { FollowingList } from '../followingList/followingList';
import { YouList } from '../you/you';
import { NavParams, Tabs } from "ionic-angular";

@Component({
  selector: 'page-FollowTabs',
  templateUrl: 'followTabs.html'
})
export class FollowTabsPage {

  @ViewChild('followTabs') tabref: Tabs;
  selectTab: string;
  tab1Root = FollowingList;
  tab2Root = YouList;

  followTabTextYou: string;
  followTabTextFollow: string;


  constructor(public params:NavParams) {
    this.selectTab= this.params.get('select');
   
     switch (localStorage.getItem("lang")) {
      case 'hi': this.followTabTextYou = "आप जिन्हे फॉलो क्र हैं ";this.followTabTextFollow = "फ़ॉलोअर्स";
        break;
      case 'en': this.followTabTextYou = "FOLLOWING";this.followTabTextFollow = "FOLLOWERS";
    }
  }

  ionViewDidEnter(){
    if(this.selectTab == "YouList")
      {
        console.log("in youlist");
        this.tabref.select(1);
      }
  }
}
