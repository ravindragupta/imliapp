import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
// import { LoginPage } from '../loginPage/loginPage';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { SignUpPage } from '../signUpPage/signUpPage';

@Component({
    selector: 'page-chooseLang',
    templateUrl: 'chooseLang.html'
})

export class ChooseLangPage {

    language: FormGroup;
    getStarted: String;
    hindi: string;
    relationship: any;
    manualTextSize: string;

    constructor(public navCntl: NavController, public formBuilder: FormBuilder){
        // this.formCode();
    }

    // formCode(){
    //     this.language = this.formBuilder.group({
    //         lang: ['',[Validators.required]]
    //     })
    // }
 
    navigateLoginPage(){
        this.navCntl.push(SignUpPage);
    }

    checkLang(event){
        switch(event){
           case 'hi': this.getStarted = 'शुरू करें';this.manualTextSize = "setFontSize";
           break;
           case 'en' : this.getStarted = 'GET STARTED';this.manualTextSize = ""
        }
        console.log("radio", event);
        let langs = event;
        localStorage.setItem("lang",langs);
    }
}
