import { Component } from '@angular/core';

import { TrendingFeed } from '../trending-feed/trending-feed';
import { FollowFeed } from '../follow-feed/follow-feed';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class HomeTabsPage {

  tab1Root = FollowFeed;
  tab2Root = TrendingFeed;
  followFeedText: string;
  trendingText: string;

  constructor(private ga: GoogleAnalytics) {
    switch (localStorage.getItem("lang")) {
      case 'hi':
        this.followFeedText = "मेरा मन";
        this.trendingText = "लोकप्रिय";
        break;
      case 'en': this.followFeedText = "Follow Feed";
        this.trendingText = "Trending";
        break;
    }
    this.ga.startTrackerWithId('UA-103017766-1')
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackView('Home');
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));

  }
}
