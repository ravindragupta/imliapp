import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { GlobalService } from '../../app.service';
import { ToastController, NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'feedback-page',
    templateUrl: './feedback.html'
})

export class FeedbackPage {

    feedbackFrom: FormGroup;
    userId: string;
    loader: any;
    translate: any = { name: '', later: '', submit: '' };

    constructor(private ga: GoogleAnalytics, public navCntrl: NavController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, private base_path_service: GlobalService, public formBuilder: FormBuilder) {
        this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter Feedback page', 'Feedback', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        switch (localStorage.getItem("lang")) {
            case 'hi': this.translate.name = "प्रतिक्रिया"; this.translate.later = "हमें अपने अनुभव के बारे में बताएं";
                this.translate.submit = 'जमा करें';
                break;
            case 'en': this.translate.name = "Feedback"; this.translate.later = "Let us know your Experience with us";
                this.translate.submit = 'SUBMIT';
                break;
        }
        this.userId = localStorage.getItem("userId");
        this.formCode();
    }

    formCode() {
        this.feedbackFrom = this.formBuilder.group({
            message: ['', Validators.required]
        })
    }

    presentLoader() {
        this.loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        this.loader.present();
    }

    sendFeedback() {
        this.presentLoader();
        let data = {
            feedback: this.feedbackFrom.controls['message'].value,
            key: this.userId
        }
        let url = this.base_path_service.base_path + 'user/feedback';
        this.base_path_service.PostRequest(url, data)
            .subscribe(data => {
                this.ga.startTrackerWithId('UA-103017766-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackEvent('Feedback page -- recieve feedback', 'Recieve New feedback');
                    })
                    .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
                this.loader.dismiss();
                let toast = this.toastCtrl.create({
                    message: 'Your feedback has been sent',
                    position: 'top',
                    duration: 3000
                });
                toast.present();
                this.feedbackFrom.reset();
                setTimeout(() => {
                    this.navCntrl.pop();
                }, 3000)
                console.log("create post", data);
            },
            err => {
                console.log('some error', err);
            });
    }

}