import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import { CommentPage } from '../commentPage/commentPage';
import { FollowList } from '../followList/followList';
import { SocialSharing } from '@ionic-native/social-sharing';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  selector: 'page-follow-feed',
  templateUrl: 'follow-feed.html',
})

export class FollowFeed {

  flowersData: any;
  userId: any;
  followData: any[];
  manualArr
  display: boolean = false;
  like: boolean = false;
  save: boolean = false;
  isSave: boolean = false;
  postId: string;
  followingCount: any;
  followingText: string;
  following: string;
  totalCount: number;
  page: number = 1;
  likeTxt: string;
  viewAllcmt: string;
  tippdi: string;
  shareTx: string;
  dummy: string = "dont";
  viewTxt: string;
  eventItemText: any = { like: '', comment: '', share: '', save: '' };

  constructor(private ga: GoogleAnalytics, private socialSharing: SocialSharing, public base_path_service: GlobalService, public modalCtrl: ModalController, private alertCtrl: AlertController, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams) {
    switch (localStorage.getItem("lang")) {
      case 'hi': this.viewTxt = 'देखने वाले', this.shareTx = " शेयर "; this.viewAllcmt = " सभी को देखें "; this.tippdi = " टिप्पणियाँ "; this.likeTxt = "को यह पसंद है"; this.followingText = "जिन लोगों को आप फॉलो कर रहे हैं उनके पोस्ट"; this.following = "फोल्लोविंग";
        this.eventItemText.like = 'लाइक'; this.eventItemText.share = 'शेयर करें'; this.eventItemText.save = 'सेव करें';
        this.eventItemText.comment = 'कहिये';
        break;
      case 'en': this.viewTxt = 'views', this.shareTx = " Share "; this.viewAllcmt = " View all "; this.tippdi = " Comments "; this.likeTxt = "Likes"; this.followingText = "Posts of people you are following."; this.following = "Following";
        this.eventItemText.like = 'Like'; this.eventItemText.share = 'Share'; this.eventItemText.save = 'Save';
        this.eventItemText.comment = 'Comment';
    }
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    console.log("ravindra 123");
    loader.present();
    this.userId = localStorage.getItem("userId");
    this.searchUserList();
    this.ga.startTrackerWithId('UA-103017766-1')
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackView('Follow Feed Page', 'follow-feed.html', true);
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));
  }

  viewFollowList() {
    // console.log(event._id, "asdd");
    // localStorage.setItem("postId", event._id);
    let modal = this.modalCtrl.create(FollowList);
    modal.present();
  }
  // viewFollowList() {
  //   this.navCtrl.pop(); 
  //   this.navCtrl.setRoot(FollowList);
  // }

  shareViaWhatsApp(data) {
    this.ga.startTrackerWithId('UA-103017766-1')
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackEvent('Follow feed -- Whats app share', 'Share', 'share post', 42);
      })
      .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
    console.log("call share", data.resource);
    this.socialSharing.shareViaWhatsApp('Imli khatta meetha', data.resource, data.resource).then(res => {
      console.log("res", res);
    }).catch((err) => {
      console.log(err, "some error");
    });
  }



  ionViewDidEnter() {
    this.followData = [];
    this.page = 1;
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    loader.present();
    console.log('ionViewDidLoad FollowFeed', this.followData);
    this.searchUserList(this.dummy);
  }

  presentModal(event) {
    console.log(event._id, "asdd");
    localStorage.setItem("postId", event._id);
    let modal = this.modalCtrl.create(CommentPage);
    modal.present();
  }



  // presentModal2() {
  //   let modal = this.modalCtrl.create(followingListPage);
  //   modal.present();
  // }

  doInfinite(event) {
    console.log("on bottom scroll", event);
    if (this.page < this.totalCount) {
      this.page++;
      this.searchUserList(event);
    }
    else
      event.enable(false);
  }

  doRefresh(event) {
    console.log("check on pull", event);
    let url = this.base_path_service.base_path + 'user/viewFollowersData/' + this.userId + '?limit=10&page=' + 1;
    this.base_path_service.GetRequest(url)
      .subscribe(data => {
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Refresh -- follow feed page', 'load new data', 'complete loading', 42);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        // console.log("followers list", data);
        this.followData = data[0].json.data.data1;
        setTimeout(() => {
          // console.log('Async operation has ended');
          event.complete();
        }, 2000);
      },
      err => {
        console.log("some error", err);
      })
  }

  searchUserList(event?) {
    console.log(this.page, "check page");
    let url = this.base_path_service.base_path + 'user/viewFollowersData/' + this.userId + '?limit=10&page=' + this.page;
    this.base_path_service.GetRequest(url)
      .subscribe(data => {
        console.log("followers list", data);
        let arr = data[0].json.data.data1;
        if (this.page == 1) {
          this.followData = data[0].json.data.data1;
        }
        else {
          this.followData = this.followData.concat(arr);
          console.log(event, "asdad");
          if (event == 'dont') {
            return;
          }
          else {
            event.complete();
          }
        }
        this.followingCount = data[0].json.data.followingCount;
        this.totalCount = data[0].json.data.totalPages;
        console.log(this.totalCount, "page count");
        console.log(data[0].json.data, "ft value");
        console.log(this.followData, "sc");
        // this.like = false;
        // this.save = false;
      },
      err => {
        console.log('some error', err);
      });
  }

  opneMore(event) {
    this.postId = event._id;
    if (event.isDelete) {
      switch (localStorage.getItem("lang")) {
        case 'hi': this.openSecondHindi(event);
          break;
        case 'en': this.openSecond(event);
          break;
      }
    }
    else {
      switch (localStorage.getItem("lang")) {
        case 'hi': this.openThirdHindi(event);
          break;
        case 'en': this.openThird(event);
          break;
      }
    }
    console.log("check boolean", event.isDelete);
  }

  openSecond(event) {
    let prompt = this.alertCtrl.create({
      title: 'Action',
      buttons: [
        {
          text: 'Share on facebook',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Report',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        },
        {
          text: 'Delete',
          handler: data => {
            this.deletePost(event);
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  openThird(event) {
    let prompt = this.alertCtrl.create({
      title: 'Action',
      buttons: [
        {
          text: 'Share on facebook',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Report',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  // ================== hindi ======================
  openSecondHindi(event) {
    let prompt = this.alertCtrl.create({
      title: 'मेनू',
      buttons: [
        {
          text: 'फेसबुक पर शेयर करें',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'रिपोर्ट',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        },
        {
          text: 'डिलीट',
          handler: data => {
            this.deletePost(event);
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  openThirdHindi(event) {
    let prompt = this.alertCtrl.create({
      title: 'मेनू',
      buttons: [
        {
          text: 'फेसबुक पर शेयर करें',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'रिपोर्ट',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }
  // =============== end hindi ++++++++++++++

  showCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Reason');

    alert.addInput({
      type: 'radio',
      label: 'Spam',
      value: 'spam',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Noneveg',
      value: 'noneveg'
    });

    alert.addInput({
      type: 'radio',
      label: 'Not intrested',
      value: 'notIntrested'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Okay',
      handler: data => {
        console.log('Checkbox data:', data);
        this.report(data)
      }
    });
    alert.present();
  }

  report(reason) {
    let data = {
      postId: this.postId,
      type: reason,
      key: this.userId
    }
    let url = this.base_path_service.base_path + 'user/report';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent(' Follow feed -- Report post', reason, 'complete report', 12);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        console.log("set report", data);
        this.searchUserList(this.dummy);
      },
      err => {
        console.log('some error', err);
      });
  }

  savePost(event) {
    if (event.isSave) {
      event.isSave = false;
    }
    else {
      event.isSave = true;
    }
    let data = {
      key: this.userId,
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/savePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Follow feed -- Save post', 'saved', 'Saved successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        console.log("edit profle", data);
        // this.searchUserList(this.dummy);
      },
      err => {
        this.save = false;
        console.log('some error', err);
      });
  }

  onLike(event) {
    if (event.liker) {
      event.like.pop()
      event.liker = false;
    }
    else {
      event.like.push('123');
      event.liker = true;
    }
    let data = {
      key: this.userId,
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/likePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        console.log("edit profle", data);
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Follow feed -- Like post', 'LIKE', 'Like Post successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        // this.searchUserList(this.dummy);
      },
      err => {
        this.like = false;
        console.log('some error', err);
      });
  }

  deletePost(event) {
    let data = {
      key: this.userId,
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/deletePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        console.log("edit profle", data);
        this.searchUserList(this.dummy);
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Follow feed -- Delete post', 'Delete', 'Delete Post successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
      },
      err => {
        console.log('some error', err);
      });
  }

  // followingList() {
  //   this.navCtrl.setRoot(followingListPage);
  // }

}
