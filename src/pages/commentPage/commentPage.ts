import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'page-comment',
    templateUrl: 'commentPage.html'
})

export class CommentPage {

    userId: string;
    postId: string;
    comment: string;
    commentList: any;
    commentForm: FormGroup;

    constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, private base_path_service: GlobalService) {
        this.formCode();
        this.userId = localStorage.getItem("userId");
        this.postId = localStorage.getItem("postId");
        this.getCommentList();
    }

    formCode(){
        this.commentForm = this.formBuilder.group({
            commentInput: ['',[Validators.required]]
        })
    }

    popNav() {
        this.navCtrl.pop();
    }

    getCommentList() {
        let data = {
            key: this.userId,
            postId: this.postId
        }
        let url = this.base_path_service.base_path + 'user/viewComments'
        this.base_path_service.PostRequest(url, data)
            .subscribe(data => {
                console.log("create post", data);
                this.commentList = data[0].json.json().data;
                console.log("chekddff1233c", this.commentList);
            },
            err => {
                console.log('some error', err);
            });
    }

    postComment() {
        let data = {
            key: this.userId,
            post: this.postId,
            comment: this.comment
        }
        let url = this.base_path_service.base_path + 'user/comments'
        this.base_path_service.PostRequest(url, data)
            .subscribe(data => {
                console.log("create post", data);
                this.commentForm.controls['commentInput'].reset();
                this.getCommentList();
            },
            err => {
                console.log('some error', err);
            });
    }
}