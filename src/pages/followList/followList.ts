import {Component} from '@angular/core';
import { FollowTabsPage } from '../followTabs/followTabs';
import {NavController} from 'ionic-angular';

@Component({
    selector: 'page-followList',
    templateUrl: 'followList.html'
})

export class FollowList {

      rootTabs: any = FollowTabsPage;
      followersList: string;

      constructor(public navCtrl: NavController){
          switch (localStorage.getItem("lang")) {
            case 'hi': this.followersList = "फॉलोवर्स लिस्ट";
                break;
            case 'en': this.followersList = "Followers List";
                break;
        }
      }

      popNav(){
          this.navCtrl.pop();
      }

}