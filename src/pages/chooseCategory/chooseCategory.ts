import { Component } from '@angular/core';
import { NavController, LoadingController, ViewController } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import { CreatePostPage } from '../createPost/createPost';
import { TabsPage } from '../tabs/tabs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'page-chooseCategory',
    templateUrl: 'chooseCategory.html'
})

export class ChooseCategoryPage {

    lang: string;
    userId: string;
    status: string;
    imageUrl: string;
    getCategoryList: any;
    getCategoryList2:any;
    categoryName: string;
    subCategory: string;
    createPostText: string;
    postText: string;
    setStyle: string;
    setStyle1: any;
    setStyle0: any;
    setStyle2: any;
    setStyle3: any;
    categorySelected: boolean= false;
    // active: boolean = false;

    constructor(private ga: GoogleAnalytics, public loadingCtrl: LoadingController,public vc: ViewController, public navCtrl: NavController, private base_path_service: GlobalService) {
        this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter choose category page', 'Feedback', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        switch (localStorage.getItem("lang")) {
            case 'hi': this.createPostText = "कोई श्रेणी चुनें"; this.postText = "पोस्ट";
                break;
            case 'en': this.createPostText = "Choose relevent category"; this.postText = "Post";
                break;
        }
        this.lang = localStorage.getItem("lang");
        this.userId = localStorage.getItem("userId");
        this.imageUrl = localStorage.getItem("postImg");
        this.status = localStorage.getItem("postMsg");
        this.getCategory();
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 1000
        });
        loader.present();
    }

    back() {
        this.navCtrl.pop();
    }

    setBackground(index) {
        // console.log("df",first);
    }

    getCategory() {
        let url = this.base_path_service.base_path + 'admin/allCategory/' + this.userId + '?language=hi,en';
        this.base_path_service.GetRequest(url)
            .subscribe(data => {
                console.log("data", data);
                this.getCategoryList = data[0].json.hindiCategory;
                this.getCategoryList2 = data[0].json.englishCategory;
                console.log("checkkk", typeof (this.getCategoryList));
                console.log("checkkk without 145", this.getCategoryList);
            },
            err => {
                console.log('some error', err);
            });
    }

    getSubCategoryName(event, category, subCat) {
        this.categorySelected=true;
        this.categoryName = category._id;
        this.subCategory = subCat._id;
        console.log("check these value", this.categoryName, this.subCategory);
    }

    createPost() {
        let data = {
            key: this.userId,
            status: this.status,
            categoryId: this.categoryName,
            subCategoryId: this.subCategory,
            resource: this.imageUrl
        }
        let url = this.base_path_service.base_path + 'user/post?lang=' + this.lang;
        this.base_path_service.PostRequest(url, data)
            .subscribe(data => {
                this.ga.startTrackerWithId('UA-103017766-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackEvent('New Post', 'Create new post successfully');
                    })
                    .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
                console.log("create post", data);
                // this.navCtrl.setRoot(TabsPage);
                this.vc.dismiss();
            },
            err => {
                console.log('some error', err);
            });
    }
}