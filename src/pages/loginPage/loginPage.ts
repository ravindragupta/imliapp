import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { SignUpPage } from '../signUpPage/signUpPage';
import { TabsPage } from '../tabs/tabs';
import { ForgotPassPage } from '../forgotPass/forgotPass';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { GlobalService } from '../../app.service';
import { ChooseLangPage } from '../chooseLang/chooseLang';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'page-login',
    templateUrl: 'loginPage.html'
})

export class LoginPage {

    loginForm: FormGroup;
    lang: string;
    items:any;
    hideFeatures: boolean = false;
    topText: string;
    signUpText: string;
    termAndcon: string;
    loginText: string;
    usernameText: string;
    password: string;
    flag: number = 0;

    constructor(private ga: GoogleAnalytics, public toastCtrl: ToastController, public navCntl: NavController, private formBuilder: FormBuilder, public base_path_service: GlobalService) {
        this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter Login page', 'LOGIN', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        switch (localStorage.getItem("lang")) {
            case 'hi': this.topText = 'मुफ़्त खाता बनाने के लिए केवल कुछ मिनट लगते हैं खाता नहीं है?';
                this.signUpText = "साइन अप करें";
                this.termAndcon = "लॉगिन करके, आप सेवा की शर्तें और गोपनीयता नीति के लिए सहमत हैं।"
                this.loginText = "लॉग इन करें";
                this.usernameText = "अपना मोबाइल नंबर डालें";
                this.password = 'पासवर्ड';
                break;
            case 'en': this.topText = "It only takes a couple of minute to create a free account. Don't have an account?";
                this.signUpText = "Sign up";
                this.termAndcon = "By login up, you agree to the imli Terms of Service and Privacy Policy.";
                this.loginText = "LOGIN";
                this.usernameText = "Mobile No";
                this.password = 'Password';
        }
        this.formCode();
        this.login();
    }

    formCode() {
        this.loginForm = this.formBuilder.group({
            mobile: ['', [Validators.required]],
            password: ['', [Validators.required]]
        })
    }

    navigateSignUppage() {
        this.navCntl.push(SignUpPage);
    }

    back() {
        this.navCntl.push(SignUpPage);
    }

    navigateHomePage() {
        this.navCntl.setRoot(TabsPage);
    }

    navigateForgotPage() {
        this.navCntl.push(ForgotPassPage);
    }

    login() {
        console.log(localStorage.getItem("token"));
        if (localStorage.getItem("token") != null) {
            this.navCntl.setRoot(TabsPage);
        }
        else {
            console.log("form dta", this.loginForm.value);
            let url = this.base_path_service.base_path + 'user/login';
            this.base_path_service.PutRequestUnautorized(url, this.loginForm.value)
                .subscribe(data => {
                    localStorage.setItem("token", JSON.stringify(data[0].json.token));
                    this.ga.startTrackerWithId('UA-103017766-1')
                        .then(() => {
                            console.log('Google analytics is ready now');
                            this.ga.trackEvent('Login', 'Login Successfully');
                        })
                        .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
                    this.navCntl.setRoot(TabsPage);
                },
                err => {
                    if (this.flag > 0) {
                        let toast = this.toastCtrl.create({
                            message: "Password or mobile no. doesn't match",
                            position: 'top',
                            duration: 2000
                        });
                        toast.present();
                    } else {
                        this.flag++;
                    }
                    console.log('some error', err);
                });
        }

    }
}