import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CommentPage } from '../commentPage/commentPage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  selector: 'page-trending-one',
  templateUrl: 'trending-one.html',
})

export class TrendingOnePage {

  myData: any;
  userId: any;
  display: boolean = false;
  like: boolean = false;
  isSave: boolean = false;
  category: string;
  subCategory: string;
  trending: any;
  postId: any;
  likeTxt: string;
  viewAllcmt: string;
  tippdi: string;
  shareTx: string;
  page: number = 1;
  totalPage: number;
  dummy: string = 'dont';
  viewTxt: string;
  eventItemText: any = { like: '', comment: '', share: '', save: '' };

  constructor(private ga: GoogleAnalytics, public modalCtrl: ModalController, private socialSharing: SocialSharing, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, private base_path_service: GlobalService) {
    switch (localStorage.getItem("lang")) {
      case 'hi': this.viewTxt = 'देखने वाले', this.shareTx = " शेयर "; this.viewAllcmt = " सभी को देखें "; this.tippdi = " टिप्पणियाँ "; this.likeTxt = "को यह पसंद है";
        this.eventItemText.like = 'लाइक'; this.eventItemText.share = 'शेयर करें'; this.eventItemText.save = 'सेव करें';
        this.eventItemText.comment = 'कहिये';
        break;
      case 'en': this.viewTxt = 'views', this.shareTx = " Share "; this.viewAllcmt = " View all "; this.tippdi = " Comments "; this.likeTxt = "Likes";
        this.eventItemText.like = 'Like'; this.eventItemText.share = 'Share'; this.eventItemText.save = 'Save';
        this.eventItemText.comment = 'Comment';
    }
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    loader.present();
    this.userId = localStorage.getItem("userId");
    this.category = localStorage.getItem("category");
    this.subCategory = localStorage.getItem("subCategory");
    this.getMyPost();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrendingOnePage');
  }

  ionViewDidEnter() {
    this.page = 1;
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    loader.present();
    this.userId = localStorage.getItem("userId");
    this.getMyPost(this.dummy);
  }

  presentModal(event) {
    console.log(event._id, "asdd");
    localStorage.setItem("postId", event._id);
    let modal = this.modalCtrl.create(CommentPage);
    modal.present();
  }

  doInfinite(event) {
    console.log("on bottom scroll", event);
    if (this.page < this.totalPage) {
      this.page++;
      this.getMyPost(event);
    }
    else
      event.enable(false);
  }


  doRefresh(event) {
    let data = {
      key: this.userId,
      categoryId: this.category,
      subCategoryId: this.subCategory
    }
    console.log("check on pull", event);
    let url = this.base_path_service.base_path + 'user/particularSortDataTrending?limit=10&page=' + 1;
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Refresh Category trending feed page', 'load new data', 'complete loading', 42);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        // console.log("followers list", data);
        this.trending = data[0].json.json().data;
        setTimeout(() => {
          // console.log('Async operation has ended');
          event.complete();
        }, 2000);
      },
      err => {
        console.log("some error", err);
      })
  }

  getMyPost(event?) {
    let data = {
      key: this.userId,
      categoryId: this.category,
      subCategoryId: this.subCategory
    }
    let url = this.base_path_service.base_path + 'user/particularSortDataTrending?limit=10&page=' + this.page;
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        let arr = data[0].json.json().data;
        if (this.page == 1) {
          console.log("feed data");
          this.trending = data[0].json.json().data;
        }
        else {
          this.trending = this.trending.concat(arr);
          if (event == 'dont') {
            return;
          }
          else {
            event.complete();
          }
        }
        this.totalPage = data[0].json.json().totalPages;
        // this.like = false;
        // this.isSave = false;
        console.log("trending", data[0].json.json().data);
        console.log("trending", typeof (data[0].json.data));
      },
      err => {
        console.log('some error', err);
      });
  }

  shareViaWhatsApp(data) {
    console.log("call share", data.resource);
    this.socialSharing.shareViaWhatsApp('Imli khatta meetha', data.resource, data.resource).then(res => {
      console.log("res", res);
    }).catch((err) => {
      console.log(err, "some error");
    });
  }

  // presentModal(event) {
  //   console.log(event._id, "asdd");
  //   localStorage.setItem("postId", event._id);
  //   let modal = this.modalCtrl.create(CommentModel);
  //   modal.present();
  // }

  savePost(event) {
    if (event.isSave) {
      event.isSave = false;
    }
    else {
      event.isSave = true;
    }
    let data = {
      key: this.userId,
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/savePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Category Trending feed Save post', 'saved', 'Saved successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        console.log("edit profle", data);
        // this.getMyPost(this.dummy);
      },
      err => {
        this.isSave = false;
        console.log('some error', err);
      });
  }

  opneMore(event) {
    this.postId = event._id;
    if (event.isDelete) {
      switch (localStorage.getItem("lang")) {
        case 'hi': this.openSecondHindi(event);
          break;
        case 'en': this.openSecond(event);
          break;
      }
    }
    else {
      switch (localStorage.getItem("lang")) {
        case 'hi': this.openThirdHindi(event);
          break;
        case 'en': this.openThird(event);
          break;
      }
    }
    console.log("check boolean", event.isDelete);
  }

  openSecond(event) {
    let prompt = this.alertCtrl.create({
      title: 'Action',
      buttons: [
        {
          text: 'Share on facebook',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Report',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        },
        {
          text: 'Delete',
          handler: data => {
            this.deletePost(event);
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  openThird(event) {
    let prompt = this.alertCtrl.create({
      title: 'Action',
      buttons: [
        {
          text: 'Share on facebook',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Report',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  // ================== hindi ======================
  openSecondHindi(event) {
    let prompt = this.alertCtrl.create({
      title: 'मेनू',
      buttons: [
        {
          text: 'फेसबुक पर शेयर करें',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'रिपोर्ट',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        },
        {
          text: 'डिलीट',
          handler: data => {
            this.deletePost(event);
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }

  openThirdHindi(event) {
    let prompt = this.alertCtrl.create({
      title: 'मेनू',
      buttons: [
        {
          text: 'फेसबुक पर शेयर करें',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'रिपोर्ट',
          handler: data => {
            this.showCheckbox();
            console.log('Deleted');
          }
        }
      ]
    });
    prompt.present();
  }
  // =============== end hindi ++++++++++++++
  showCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Reason');

    alert.addInput({
      type: 'radio',
      label: 'Spam',
      value: 'spam',
      checked: true
    });

    alert.addInput({
      type: 'radio',
      label: 'Noneveg',
      value: 'noneveg'
    });

    alert.addInput({
      type: 'radio',
      label: 'Not intrested',
      value: 'notIntrested'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Okay',
      handler: data => {
        console.log('Checkbox data:', data);
        this.report(data)
      }
    });
    alert.present();
  }

  report(reason) {
    let data = {
      postId: this.postId,
      type: reason,
      key: this.userId
    }
    let url = this.base_path_service.base_path + 'user/report';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Category Trending feed Report post', reason, 'complete report', 12);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        console.log("set report", data);
        this.getMyPost(this.dummy);
      },
      err => {
        console.log('some error', err);
      });
  }

  deletePost(event) {
    let data = {
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/deletePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Category Trending feed Delete post', 'Delete', 'Delete Post successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        console.log("edit profle", data);
        this.getMyPost();
      },
      err => {
        console.log('some error', err);
      });
  }

  onLike(event) {
    if (event.liker) {
      event.like.pop();
      event.liker = false;
    }
    else {
      event.like.push('123');
      event.liker = true;
    }
    let data = {
      key: this.userId,
      post: event._id
    }
    let url = this.base_path_service.base_path + 'user/likePost';
    this.base_path_service.PostRequest(url, data)
      .subscribe(data => {
        this.ga.startTrackerWithId('UA-103017766-1')
          .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackEvent('Category Trending feed Like post', 'LIKE', 'Like Post successfully', 13);
          })
          .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
        console.log("edit profle", data);
        // this.getMyPost(this.dummy);
      },
      err => {
        this.like = false;
        console.log('some error', err);
      });
  }

}
