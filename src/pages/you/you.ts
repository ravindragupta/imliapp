import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { GlobalService } from '../../app.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'page-you',
    templateUrl: 'you.html'
})

export class YouList {

    userId: any;
    followingData: any;
    unfollowBtn: boolean = true;
    followBtn: boolean = false;
    followText: string;
    unfollowText: string;

    constructor(private ga: GoogleAnalytics, public modalCtrl: ModalController, private alertCtrl: AlertController, public loadingCtrl: LoadingController, private base_path_service: GlobalService, public navCtrl: NavController, public navParams: NavParams) {
        switch (localStorage.getItem("lang")) {
            case 'hi': this.followText = " फॉलो"; this.unfollowText = " अनफॉलो";
                break;
            case 'en': this.followText = " Follow"; this.unfollowText = " Unfollow";
                break;
        }
        this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter You page', 'See your following list', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 2000
        });
        loader.present();
        this.userId = localStorage.getItem("userId");
        this.followingList();
    }

    followingList() {
        let url = this.base_path_service.base_path + 'user/viewFollowing/' + this.userId;
        this.base_path_service.GetRequest(url)
            .subscribe(data => {
                console.log("following list", data);
                this.followingData = data[0].json.data;
                this.followingData.forEach((element) => {
                    element.isFollowing = true;
                })
            },
            err => {
                console.log('some error', err);
            });
    }

    popNav() {
        this.navCtrl.pop();
    }

    follow(data, flag, index) {
        console.log(data.isFollowing, flag, index, "sd");
        switch (flag) {
            case 'follow': data.isFollowing = true;
                break;
            case 'unfollow': data.isFollowing = false;
                break;
        }
        let data1 = {
            key: this.userId,
            followingId: data.followingId
        }
        let url = this.base_path_service.base_path + 'user/following';
        this.base_path_service.PostRequest(url, data1)
            .subscribe(data => {
                console.log("follow list", data);
            },
            err => {
                switch (name) {
                    case 'follow': this.unfollowBtn = true; this.followBtn = true;
                        break;
                    case 'unfollow': this.followBtn = false; this.unfollowBtn = true;
                        break;
                }
                console.log('some error', err);
            });
    }
}