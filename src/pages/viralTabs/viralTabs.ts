import { Component } from '@angular/core';
import { FreshPage } from '../fresh/fresh';
import { BestProfilePage } from '../best-profile/best-profile';
import { TrendingOnePage } from '../trending-one/trending-one';

@Component({
    selector: 'page-viralTabs',
    templateUrl: 'viralTabs.html'
})

export class ViraltabsPage {

    tabText: any = { trending: '', fresh: '' }

    tab1Root = TrendingOnePage;
    tab2Root = BestProfilePage;
    tab3Root = FreshPage;

    constructor() {
        switch (localStorage.getItem("lang")) {
            case 'hi': this.tabText.trending = "लोकप्रिय"; this.tabText.fresh = "ताज़ा";
                break;
            case 'en': this.tabText.trending = "Trending"; this.tabText.fresh = "Fresh";
                break;
        }
    }

}