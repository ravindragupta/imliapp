import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../loginPage/loginPage';
import { GlobalService } from '../../app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChooseLangPage } from '../chooseLang/chooseLang';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
    selector: 'page-signUp',
    templateUrl: 'signUpPage.html'
})

export class SignUpPage {

    signUpForm: FormGroup;
    types: any[];
    lang: string;
    eighteenText: string;
    translate: any = { term: '', signUp: '', longText: '', loginTxt: '', genderText: '', username: '', email: '', mobileNo: '', password: '' };
    error: any = { name: '', email: '', mobile: '', maxLength: '', minLength: '' };

    constructor(private ga: GoogleAnalytics, public navCntl: NavController, public formBuilder: FormBuilder, public navCtrl: NavController, private base_path_service: GlobalService) {
        this.ga.startTrackerWithId('UA-103017766-1')
            .then(() => {
                console.log('Google analytics is ready now');
                this.ga.trackView('Enter Sign up page', 'create account', true);
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        switch (localStorage.getItem("lang")) {
            case 'hi': this.selectButton2(); this.eighteenText = '18+ पोस्ट देखें'; this.translate.signUp = "नया अकाउंट बनाये"; this.translate.longText = 'मुफ़्त खाता बनाने के लिए इसमें कुछ मिनट लगते हैं। खाता नहीं है?';
                this.translate.username = 'आपका पूरा नाम'; this.translate.email = "ईमेल"; this.translate.mobileNo = "मोबाइल नंबर";
                this.translate.password = "पासवर्ड"; this.translate.term = "लॉगिन करके, आप सेवा की शर्तें और गोपनीयता नीति के लिए सहमत हैं।";
                this.error.name = 'केवल अल्फाबेट'; this.error.email = 'मान्य ईमेल पता दर्ज करें';
                this.error.mobile = 'मान्य मोबाइल नंबर दर्ज करें'; this.translate.loginTxt = "लॉग इन करें";
                this.error.maxLength = "अधिकतम लंबाई 10 अंक है"; this.error.minLength = "न्यूनतम लंबाई 10 अंक है";
                break;
            case 'en': this.selectButton(); this.eighteenText = 'See 18+ post'; this.translate.signUp = "SIGN UP"; this.translate.longText = "It only takes a couple of minute to create a free account. Don't have an account?";
                this.translate.username = 'Full Name'; this.translate.email = "Email"; this.translate.mobileNo = "Mobile No";
                this.translate.password = "Password"; this.translate.term = "By login up, you agree to the imli Terms of Service and Privacy Policy.";
                this.error.name = 'Only Alphabet'; this.error.email = 'Enter valid email address';
                this.error.mobile = 'Enter valid mobile number'; this.translate.loginTxt = "Login";
                this.error.maxLength = "Maximum 10 Digit"; this.error.minLength = "Minimum 10 Digit";
        }
        this.lang = localStorage.getItem("lang");
        this.formCode();
    }

    formCode() {
        this.signUpForm = this.formBuilder.group({
            name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
            // email: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-z_.0-9]+@[a-zA-Z]+[.][a-zA-Z.]+')])],
            mobile: ['', Validators.compose([Validators.required, Validators.pattern('^(?!0+$)[0-9]+$'), Validators.minLength(10), Validators.maxLength(10)])],
            password: ['', [Validators.required]],
            // prefix: ['', [Validators.required]],
            language: this.lang,
            eighteenPlus: false
        })
    }

    back() {
        this.navCntl.push(ChooseLangPage);
    }

    selectButton() {
        this.types = [];
        this.types.push({ label: 'Male', value: 'male' });
        this.types.push({ label: 'Female', value: 'female' });
    }

    selectButton2() {
        this.types = [];
        this.types.push({ label: 'पुरष', value: 'male' });
        this.types.push({ label: 'स्त्री', value: 'female' });
    }


    navigateLoginPage() {
        this.navCtrl.setRoot(LoginPage);
    }

    // signUp() {
    //     console.log("check data", this.signUpForm.value);
    // }

    signUp() {
        console.log("form dta", this.signUpForm.value);
        let url = this.base_path_service.base_path + 'user/register';
        this.base_path_service.PutRequestUnautorized(url, this.signUpForm.value)
            .subscribe(data => {
                this.ga.startTrackerWithId('UA-103017766-1')
                    .then(() => {
                        console.log('Google analytics is ready now');
                        this.ga.trackEvent('Sign up page -- Create a new account', 'New account');
                    })
                    .catch(e => console.log('Error trackEvent starting GoogleAnalytics', e));
                localStorage.setItem("token", JSON.stringify(data[0].json.token));
                console.log("data", data[0].json.token);
                this.navCtrl.push(LoginPage);
            },
            err => {
                console.log('some error', err);
            });
    }
}