import { Component, ViewChild } from "@angular/core";
import { Nav, Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { TabsPage } from "../pages/tabs/tabs";
import { Network } from "@ionic-native/network";
import { AlertController, ViewController } from "ionic-angular";
import { ChooseLangPage } from "../pages/chooseLang/chooseLang";
import { GoogleAnalytics } from "@ionic-native/google-analytics";
// import { TabsPage } from '../pages/tabs/tabs';

@Component({
  templateUrl: "app.html"
})
export class Imli {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(ViewController) viewCtrl: ViewController; 

  rootPage: any;
  alert: any;
  networkTitle: string;
  networkSub: string;
  retryTxt: string;
  index: number = 0;

  constructor(
    private ga: GoogleAnalytics,
    public alertCtrl: AlertController,
    private network: Network,
    private platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      platform.registerBackButtonAction(() => {
        if (this.nav.canGoBack()) {
          this.nav.pop();
        } else {
          if (this.alert) {
            this.alert.dismiss();
            this.alert = null;
          } else {
            if((this.viewCtrl.component.name)=="HomePage")
              this.showAlert();
            else
              this.nav.push(TabsPage,{index:"0"});
          }
        }
      });
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.ga
      .startTrackerWithId("UA-103017766-1")
      .then(() => {
        console.log("Google analytics is ready now 2");
        this.ga.trackView("Home", "app.html", true);
      })
      .catch(e => console.log("Error starting GoogleAnalytics 12", e));

    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log("network was disconnected :-(");
      console.log("check net", this.network.type);
      console.log("check net", this.network);
      switch (localStorage.getItem("lang")) {
        case "hi":
          this.presentAlert1();
          break;
        case "en":
          this.presentAlert();
          break;
      }
    });

    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log("network connected!");
      this.alert.dismiss();
      console.log("check net", this.network.type);
      console.log("check net", this.network);

      setTimeout(() => {
        if (this.network.type === "wifi") {
          console.log("we got a wifi connection, woohoo!");
          this.alert.dismiss();
        }
      }, 3000);
    });

    if (localStorage.getItem("token") != null) {
      this.rootPage = TabsPage;
    } else {
      this.rootPage = ChooseLangPage;
    }
  }

  presentAlert() {
    this.alert = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: "Please check your data connection",
      buttons: [
        {
          text: "Retry",
          handler: data => {
            if (this.network.type == "none") {
              this.presentAlert();
              console.log("dfsfd", this.network.type);
              console.log("without type", this.network);
            } else {
              console.log("dfsfd", this.network.type);
              this.alert.dismiss();
            }
          }
        }
      ]
    });
    this.alert.present();
  }

  presentAlert1() {
    this.alert = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: "कृपया अपना इंटरनेट कनेक्शन चेक करें",
      buttons: [
        {
          text: "फिर से कोशिश करें",
          handler: data => {
            if (this.network.type == "none") {
              this.presentAlert1();
              console.log("dfsfd", this.network.type);
              console.log("without type", this.network);
            } else {
              console.log("dfsfd", this.network.type);
              this.alert.dismiss();
            }
          }
        }
      ]
    });
    this.alert.present();
  }

  showAlert() {
    this.alert = this.alertCtrl.create({
      title: "Exit?",
      message: "Do you want to exit the app?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.alert = null;
          }
        },
        {
          text: "Exit",
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.alert.present();
  }
}
