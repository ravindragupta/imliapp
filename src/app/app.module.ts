import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Imli } from './app.component';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { SearchPage } from '../pages/search/search';
import { SavedPostPage } from '../pages/savedPost/savedPost';
import { MyPostPage } from '../pages/myPost/myPost';
import { TrendingFeed } from '../pages/trending-feed/trending-feed';
import { FollowFeed } from '../pages/follow-feed/follow-feed';
import { TabsPage } from '../pages/tabs/tabs';
import { HomeTabsPage } from '../pages/homeTabs/tabs';
import { GlobalService } from '../app.service';
import { SearchUser } from '../pages/searchUser/searchUser';
import { EditProfilePage } from '../pages/editProfile/editProfile';
import { CreatePostPage } from '../pages/createPost/createPost';
import { ChooseCategoryPage } from '../pages/chooseCategory/chooseCategory';
import { ChooseLangPage } from '../pages/chooseLang/chooseLang';
import { LoginPage } from '../pages/loginPage/loginPage';
import { SignUpPage } from '../pages/signUpPage/signUpPage';
import { ForgotPassPage } from '../pages/forgotPass/forgotPass';
import { SeePostByCategoryPage } from '../pages/seePostByCategory/seePostByCategory';
import { TrendingOnePage } from '../pages/trending-one/trending-one';
import { FreshPage } from '../pages/fresh/fresh';
import { BestProfilePage } from '../pages/best-profile/best-profile';
import { ViraltabsPage } from '../pages/viralTabs/viralTabs';
import { CommentPage } from '../pages/commentPage/commentPage';
import { SettingsPage } from '../pages/settings/settings';
import { FollowList } from '../pages/followList/followList';
import { FollowTabsPage } from '../pages/followTabs/followTabs';
import { FollowingList } from '../pages/followingList/followingList';
import { YouList } from '../pages/you/you';
import {ChangePasswordComponent} from '../pages/change-password/change-password';
// import { AUTH_PROVIDERS, JwtHelper } from 'angular2-jwt';
import { Network } from '@ionic-native/network';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"
import { PanelModule } from '../primeng-master/components/panel/panel';
import { SelectButtonModule } from 'primeng/primeng';
import { SocialSharing } from '@ionic-native/social-sharing';
import {EditorModule,SharedModule} from 'primeng/primeng';
import {FeedbackPage} from '../pages/feedback/feedback';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@NgModule({
  declarations: [
    ViraltabsPage,
    TrendingOnePage,
    FreshPage,
    BestProfilePage,
    Imli,
    AboutPage,
    HomePage,
    TabsPage,
    SearchPage,
    SavedPostPage,
    TrendingFeed,
    FollowFeed,
    HomeTabsPage,
    SearchUser,
    EditProfilePage,
    CreatePostPage,
    ChooseCategoryPage,
    ChooseLangPage,
    LoginPage,
    SignUpPage,
    ForgotPassPage,
    SeePostByCategoryPage,
    CommentPage,
    SettingsPage,
    FollowList,
    FollowTabsPage,
    FollowingList,
    YouList,
    ChangePasswordComponent,
    FeedbackPage,
    MyPostPage
  ],
  imports: [
    FormsModule,
    SelectButtonModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    PanelModule,
    CommonModule,
    BrowserModule,
    EditorModule,SharedModule,
    IonicModule.forRoot(Imli)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Imli,
    AboutPage,
    HomePage,
    TabsPage,
    SearchPage,
    SavedPostPage,
    TrendingFeed,
    FollowFeed,
    HomeTabsPage,
    SearchUser,
    EditProfilePage,
    CreatePostPage,
    ChooseCategoryPage,
    ChooseLangPage,
    LoginPage,
    SignUpPage,
    ForgotPassPage,
    SeePostByCategoryPage,
    ViraltabsPage,
    TrendingOnePage,
    FreshPage,
    BestProfilePage,
    CommentPage,
    SettingsPage,
    FollowList,
    FollowTabsPage,
    FollowingList,
    YouList,
    ChangePasswordComponent,
    FeedbackPage,
    MyPostPage
  ],
  providers: [
    GoogleAnalytics,
    Network,
    SocialSharing, 
    GlobalService,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})

export class AppModule { }
